import pandas
import math
import numpy as np
from datetime import datetime
from surprise import SVD
from surprise import Reader
from surprise import Dataset
from surprise import accuracy
from surprise.model_selection import train_test_split
from surprise.model_selection import GridSearchCV
from collections import defaultdict
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
import sklearn
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error


def prepare_movielens_data(data_path):
    """
    :param data_path: The address of the data
    :return: The trainset, testset and the merged data and the data about user's gender and the release year of movie
    """

    # get user gender, index user ids from 0 to (#user - 1)
    users = pandas.read_csv(data_path + 'u.user', sep='|', header=None, names=['id', 'age', 'gender', 'occupation', 'zip-code'])
    gender = pandas.DataFrame(users['gender'].apply(lambda x: int(x == 'M'))) # convert F/M to 0/1
    user_id = dict(zip(users['id'], range(users.shape[0]))) # mapping user id to linear index

    # the zero-th column is the id, and the second column is the release date
    movies = pandas.read_csv(data_path + 'u.item', sep='|', encoding = 'latin-1', header=None, usecols=[0, 1, 2],
                             names=['item-id', 'title', 'release-year'])

    bad_movie_ids = list(movies['item-id'].loc[movies['release-year'].isnull()]) # get movie ids with a bad release date

    movies = movies[movies['release-year'].notnull()] # item 267 has a bad release year, remove this item
    release_year = pandas.DataFrame(movies['release-year'].apply(lambda x: datetime.strptime(x, '%d-%b-%Y').year))
    movie_id = dict(zip(movies['item-id'], range(movies.shape[0]))) # mapping movie id to linear index

    # get ratings, remove ratings of movies with bad release years.
    rating_triples = pandas.read_csv(data_path + 'u.data', sep='\t', header=None, names=['user', 'item', 'rating', 'timestamp'])
    rating_triples = rating_triples[['user', 'item', 'rating']] # drop the last column
    rating_triples = rating_triples[~ rating_triples['item'].isin(bad_movie_ids)] # drop movies with bad release years

    # map user and item ids to user indices
    rating_triples['user'] = rating_triples['user'].map(user_id)
    rating_triples['item'] = rating_triples['item'].map(movie_id)

    # the following set assertions guarantees that the user ids are in [0, #users), and item ids are in [0, #items)
    assert(rating_triples['item'].unique().min() == 0)
    assert(rating_triples['item'].unique().max() == movies.shape[0] - 1)
    assert(rating_triples['user'].unique().min() == 0)
    assert(rating_triples['user'].unique().max() == users.shape[0] - 1)
    assert(rating_triples['item'].unique().shape[0] == movies.shape[0])
    assert(rating_triples['user'].unique().shape[0] == users.shape[0])

    # training/test set split
    rating_triples = rating_triples.sample(frac=1, random_state=2018).reset_index(drop=True) # shuffle the data
    train_ratio = 0.9
    train_size = int(train_ratio * rating_triples.shape[0])

    trainset = rating_triples.loc[0:train_size]
    testset = rating_triples.loc[train_size + 1:]

    return trainset, testset, gender, release_year, rating_triples



def data_process(data):
    """
    Using to transfer the data into AutoFold form
    :param data: The trainset or testset
    :return: trainset or testset in AutoForm form.
    """
    df = pandas.DataFrame(data)
    reader = Reader(line_format='user item rating', sep=',', skip_lines=1)
    return Dataset.load_from_df(df, reader)



def SVD_Classifier_param_find(train_data):
    """
    Using GridSearchCV to find the best hyper-parameter and draw the plot
   :param train_data: The vector form of features and labels of training data
    :return: The best hyper-parameter with highest test accuracy.
   """

    # The candidate hyperparameters for model selection
    tuned_parameters = {'n_factors': [4, 6, 8, 10, 15], 'reg_all': [0.01, 0.1, 1, 10]}
    clf = GridSearchCV(SVD, tuned_parameters, cv=5, measures=['rmse'])
    clf.fit(train_data)
    print(clf.best_params['rmse'])
    return clf.best_params['rmse']



def SVD_Classifier_fit(train_data, para):
    """
    Using the best hyper-parameter to fit the model.
    :param train_data: The vector form of features and labels of all data.
    :param para: The best hyper-parameter.
    :return: The parameter of users and items.
    """
    f = SVD(**para)
    f.fit(train_data)
    U = f.pu  # user
    V = f.qi  # item
    return [U, V]



def SGD_Classifier_param_Analysis_user(X, y):
    """
    Using the parameter of users as feature to predict gender of user, to check whether the user vector contain the gender information.
    :param X: The parameter of users.
    :param y: The true label of the gender of users.
    """

    # fing the best hyper-parameter for a regression classifier
    mySGD = linear_model.SGDClassifier(loss = 'log', max_iter = 1000, tol=1e-3)
    tuned_parameters = {'alpha': [0.0000001, 0.000001, 0.00001, 0.00001, 0.0001, 10, 1000]}
    clf = sklearn.model_selection.GridSearchCV(mySGD, tuned_parameters, cv=5, scoring='accuracy', return_train_score=True)
    clf.fit(X, y)

    #draw the plot
    plt.title("SGD")
    alpha = [0.001, 0.01, 0.1, 1, 10, 100, 1000]
    test_accuracy = clf.cv_results_['mean_test_score']
    test_std = clf.cv_results_['std_test_score']
    plt.bar(list(range(len(alpha))), test_accuracy, width=0.4, tick_label = alpha, fc='r')
    plt.show()

    # The result should be writen as [accuracy - std, accuracy + std].
    for i in range(len(alpha)):
        print('When alpha = ' + str(alpha[i]) + ': I have 95% confidence that the accuracy could be' + str(test_accuracy[i]) + '+-' + str(1.96*test_std[i]))



def SGD_Classifier_param_Analysis_item(X, y):
    """
    Using the parameter of items as feature to predict the release year of items, to check whether the item vector contain the release year information.
    :param X: The parameter of items.
    :param y: The true label of the release year of items.
    """

    # fing the best hyper-parameter for a regression classifier.
    myLR = LinearRegression()
    tuned_parameters = {}
    clf = sklearn.model_selection.GridSearchCV(myLR, tuned_parameters, cv=5, scoring='neg_mean_squared_error', return_train_score=True)
    clf.fit(X, y)

    # calculate the mean squared error of the model.
    mse = 0 - clf.cv_results_['mean_test_score']
    print('MSE of the model is ' + str(mse[0]))

    #Calculate the MSE of a naive model
    print("The MSE of the naive model is " + str(np.var(y)))



if __name__ == "__main__":

    # prepare data
    print('Extracting data from the ml-100k dataset ...')
    trainset, testset, gender, release_year, rating_triples = prepare_movielens_data(data_path='../ml-100k/')
    trainset.to_csv('trainset.csv', index=False)
    testset.to_csv('testset.csv', index=False)
    gender.to_csv('gender.csv', index=False)
    release_year.to_csv('release-year.csv', index=False)

    #Get the gender data and release year data for analysis
    genders = np.array(gender).T[0]
    release = np.array(release_year).T[0]

    #preprocess the trainset and testset
    train_data = data_process(trainset)
    Data = data_process(rating_triples)

    #Using the merge data of train and test data to get U and V
    para = SVD_Classifier_param_find(Data)
    Data = Data.build_full_trainset()
    [U, V] = SVD_Classifier_fit(Data, para)

    #Analysis the user data and item data with gender and release year information.
    SGD_Classifier_param_Analysis_user(U, genders)
    SGD_Classifier_param_Analysis_item(V, release)