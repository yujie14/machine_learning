import pandas
import math
import numpy as np
from datetime import datetime
from surprise import SVD
from surprise import Reader
from surprise import Dataset
from surprise import accuracy
from surprise.model_selection import train_test_split
from surprise.model_selection import GridSearchCV
from collections import defaultdict


def prepare_movielens_data(data_path):
    """
    :param data_path: The address of the data
    :return: The trainset, testset and the data about user's gender and the release year of movie
    """

    # get user gender, index user ids from 0 to (#user - 1)
    users = pandas.read_csv(data_path + 'u.user', sep='|', header=None, names=['id', 'age', 'gender', 'occupation', 'zip-code'])
    gender = pandas.DataFrame(users['gender'].apply(lambda x: int(x == 'M'))) # convert F/M to 0/1
    user_id = dict(zip(users['id'], range(users.shape[0]))) # mapping user id to linear index

    # the zero-th column is the id, and the second column is the release date
    movies = pandas.read_csv(data_path + 'u.item', sep='|', encoding = 'latin-1', header=None, usecols=[0, 1, 2],
                             names=['item-id', 'title', 'release-year'])

    bad_movie_ids = list(movies['item-id'].loc[movies['release-year'].isnull()]) # get movie ids with a bad release date

    movies = movies[movies['release-year'].notnull()] # item 267 has a bad release year, remove this item
    release_year = pandas.DataFrame(movies['release-year'].apply(lambda x: datetime.strptime(x, '%d-%b-%Y').year))
    movie_id = dict(zip(movies['item-id'], range(movies.shape[0]))) # mapping movie id to linear index

    # get ratings, remove ratings of movies with bad release years.
    rating_triples = pandas.read_csv(data_path + 'u.data', sep='\t', header=None, names=['user', 'item', 'rating', 'timestamp'])
    rating_triples = rating_triples[['user', 'item', 'rating']] # drop the last column
    rating_triples = rating_triples[~ rating_triples['item'].isin(bad_movie_ids)] # drop movies with bad release years

    # map user and item ids to user indices
    rating_triples['user'] = rating_triples['user'].map(user_id)
    rating_triples['item'] = rating_triples['item'].map(movie_id)

    # the following set assertions guarantees that the user ids are in [0, #users), and item ids are in [0, #items)
    assert(rating_triples['item'].unique().min() == 0)
    assert(rating_triples['item'].unique().max() == movies.shape[0] - 1)
    assert(rating_triples['user'].unique().min() == 0)
    assert(rating_triples['user'].unique().max() == users.shape[0] - 1)
    assert(rating_triples['item'].unique().shape[0] == movies.shape[0])
    assert(rating_triples['user'].unique().shape[0] == users.shape[0])

    # training/test set split
    rating_triples = rating_triples.sample(frac=1, random_state=2018).reset_index(drop=True) # shuffle the data
    train_ratio = 0.9
    train_size = int(train_ratio * rating_triples.shape[0])

    trainset = rating_triples.loc[0:train_size]
    testset = rating_triples.loc[train_size + 1:]

    return trainset, testset, gender, release_year



def data_process(data):
    """
    Using to transfer the data into AutoFold form
    :param data: The trainset or testset
    :return: trainset or testset in AutoForm form.
    """
    df = pandas.DataFrame(data)
    reader = Reader(line_format='user item rating', sep=',', skip_lines=1)
    return Dataset.load_from_df(df, reader)



def SVD_Classifier_param_find(train_data):
    """
    Using GridSearchCV to find the best hyper-parameter and draw the plot
    :param train_data: The vector form of features and labels of training data
    :return: The best hyper-parameter with highest test accuracy.
    """

    #The candidate hyperparameters for model selection
    tuned_parameters = {'n_factors': [4, 6, 8, 10, 15], 'reg_all': [0.01, 0.1, 1, 10]}
    clf = GridSearchCV(SVD, tuned_parameters, cv=5, measures=['rmse'])
    clf.fit(train_data)
    print(clf.best_params['rmse'])
    return clf.best_params['rmse']



def SVD_Classifier_fit(train_data, para):
    """
    Using the best hyper-parameter to fit the model.
    :param train_data: The vector form of features and labels of training data.
    :param para: The best hyper-parameter.
    :return: The model.
    """
    f = SVD(**para)
    f.fit(train_data)
    return f



def SVD_Classifier_evaluate_MAE(model, test_data):
    """
    Using to evaluate the recommendation by calculating the MAE with the testset.
    :param model: The model achieved with the best hyperparameter and the trainset.
    :param test_data: Containing the true label, used to calculate the MAE.
    """
    predictions = model.test(test_data)
    print("The Mean Absoluate Error is " + str(accuracy.mae(predictions)))



def get_top_n(predictions, n):
    '''Return the top-N recommendation for each user from a set of predictions.

    Args:
        predictions(list of Prediction objects): The list of predictions, as
            returned by the test method of an algorithm.
        n(int): The number of recommendation to output for each user. Default
            is 10.

    Returns:
    A dict where keys are user (raw) ids and values are lists of tuples:
        [(raw item id, rating estimation), ...] of size n.
    '''

    # First map the predictions to each user.
    top_n = defaultdict(list)
    for uid, iid, true_r, est, _ in predictions:
        top_n[uid].append((iid, est))

    # Then sort the predictions for each user and retrieve the k highest ones.
    for uid, user_ratings in top_n.items():
        user_ratings.sort(key=lambda x: x[1], reverse=True)
        top_n[uid] = user_ratings[:n]
    return top_n



def SVD_Classifier_evaluate_top(model, train_data, test_data):
    """
    Using to evaluate the recommendation by recommend 5 items and calculate the true rating of the items.
    :param model: The model achieved with the best hyperparameter and the trainset.
    :param train_data: Used to find the ratings which are not in trainset.
    :param test_data: Used to find the true ratings of the recommended items, if not, the rating will be 2.
    """
    # Than predict ratings for all pairs (u, i) that are NOT in the training set.
    testset = train_data.build_anti_testset()
    predictions = model.test(testset)

    #Find top 5 recommended items
    top_n = get_top_n(predictions, n = 5)

    #Store all testset data in the dictionary
    dict = {}
    for i in test_data:
        s = str(i[0]) + ' ' + str(i[1])
        dict[s] = i[2]

    #Build a table to store all true rating of the 5 items for each user.
    table = np.zeros((943, 5))
    for uid, user_ratings in top_n.items():
        i = 0
        for (iid, _) in user_ratings:
            table[uid][i] = dict.get(str(uid) + ' ' + str(iid), 2)
            i = i + 1

    #Calculate the overall average rating.
    print("The overall average rating is " + str(np.mean(table)))



if __name__ == "__main__":
    # prepare data
    print('Extracting data from the ml-100k dataset ...')
    trainset, testset, gender, release_year = prepare_movielens_data(data_path='../ml-100k/')
    trainset.to_csv('trainset.csv', index=False)
    testset.to_csv('testset.csv', index=False)
    gender.to_csv('gender.csv', index=False)
    release_year.to_csv('release-year.csv', index=False)

    #preprocess the trainset and testset
    train_data = data_process(trainset)
    test_data = data_process(testset)

    #Find the best hyperparameters for the SVD classifier.
    para = SVD_Classifier_param_find(train_data)

    #Fit the model with the best hyperparameter and the trainset.
    train_data = train_data.build_full_trainset()
    test_data = test_data.build_full_trainset().build_testset()
    model = SVD_Classifier_fit(train_data, para)

    #evaluate the recommendation in two approaches.
    SVD_Classifier_evaluate_MAE(model, test_data)
    SVD_Classifier_evaluate_top(model, train_data, test_data)