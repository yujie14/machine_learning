import numpy as np
import random
import math
import csv
from matplotlib import pyplot as plt
from datetime import datetime

class MyLogisticReg:
    def __init__(self, num_feature):
        self.w = np.random.random((num_feature, 1))
        self.w0 = np.random.random((1, 1))
        self.Lambda = 1
        self.epsilon = 1
        self.delta = 0.000001
        self.a = []
        self.b = []
    def fit(self, X, Y):
        w_all = np.append(self.w, self.w0, axis=0)
        ori_w_all = w_all
        w_all = self.optimizer(w_all, X, Y, 1)
        w_all = ori_w_all
        w_all = self.optimizer(w_all, X, Y, 2)
        plt.title("iteration-loss plot")
        plt.xlabel("num_iteration(/100)")
        plt.ylabel("loss")
        l1, = plt.plot(self.a[1:], color='b')
        l2, = plt.plot(self.b[1:], color='r')
        plt.legend(handles = [l1,l2,], labels = ['GD', 'SGD'], loc = 'best')
        plt.show()
        self.w = w_all[:-1]
        self.w0 = w_all[-1]



    def optimizer(self, w_all, X, Y, s):
        t = 0
        while(t == 0 or np.sum(np.abs(w_all - last_w_all))/w_all.shape[0] > self.epsilon):
            last_w_all = w_all
            for i in range(1000):
                if s == 1:
                    w_all = self.gradient(w_all, X, Y, t)
                    if (i % 100 == 0):
                        print("The ", t, "-th iteration's loss is", self.loss(w_all, X, Y))
                        self.a.append(self.loss(w_all, X, Y))
                elif s == 2:
                    w_all = self.SGD_gradient(w_all, X, Y, t, 10)
                    if (i % 100 == 0):
                        print("The ", t, "-th iteration's loss is", self.loss(w_all, X, Y))
                        self.b.append(self.loss(w_all, X, Y))
                t = t + 1
            print(np.sum(np.abs(w_all - last_w_all))/w_all.shape[0])
        return w_all

    def gradient(self, w_all, X, Y, t):
        grad = self.gradi(w_all, X, Y, X.shape[0], X.shape[0])
        w_all = w_all - grad/(1+t)
        return w_all


    def SGD_gradient(self, w_all, X, Y, t, S):
        data = np.append(Y, X, axis=1)
        np.random.shuffle(data)
        X_SGD = data[0:S, 1:]
        Y_SGD = data[0:S, 0]
        grad = self.gradi(w_all, X_SGD, Y_SGD, X.shape[0], X_SGD.shape[0])
        w_all = w_all - grad / S * X.shape[0] / (1 + t)
        return w_all

    def gradi(self, w_all, X, Y, N, S):
        X = np.append(X, np.ones((X.shape[0], 1)), axis=1)
        n = np.dot(X, w_all)
        da = np.dot(Y.T, X) - np.dot(np.array(list(map(self.safe_softplus_grad, n.T[0]))), X)
        gra = (self.Lambda * S / N  * w_all.T - da).T
        return gra
    def safe_softplus_grad(self, x):
        if x > 30:
            return 1
        else:
            return np.exp(x)/(1 + np.exp(x))



    def loss(self, w_all, X, Y):
        w = w_all[:-1]
        w0 = w_all[-1]
        n = np.asarray(np.dot(X, w)) + w0
        su = np.array(list(map(self.safe_softplus_loss, n.T[0]))).sum()
        a = np.dot(Y.T, n) - su
        return (self.Lambda/2 * math.pow(np.linalg.norm(w), 2) - a)[0][0]
    def safe_softplus_loss(self, x):
        if x > 30:
            return x
        else:
            return np.log(1.0 + np.exp(x))

    def predict(self, X):
        Y_pred = np.array(list(map(self.judge, list(X))))
        return Y_pred
    def judge(self, Xi):
        if (np.dot(Xi, self.w) + self.w0) > 0:
            return 1
        else:
            return 0

    def evaluate(self, y_test, y_pred):
        accurate_rate = (np.sum(np.equal(y_test, y_pred).astype(np.float)) / y_test.size)
        print(np.sum(np.equal(y_test, y_pred).astype(np.float)))
        print(y_test.size)
        return accurate_rate

def main():
    my_matrix = np.loadtxt(open("mnist-train.csv", "rb"), delimiter=",", skiprows=1)
    X = my_matrix[0:, 1:].astype(np.float)
    Y1 = my_matrix[0:, 0].astype(np.float)
    Y1 = np.asmatrix(Y1)
    Y1 = np.asarray(Y1.T)
    y1_label = Y1[0, 0]
    for i in range(Y1.shape[0]):
        if(Y1[i,0] != y1_label):
            y2_label = Y1[i,0]
            break;
    if(y1_label < y2_label):
        temp = y1_label
        y1_label = y2_label
        y2_label = temp
    Y = (Y1 - y2_label) / (y1_label - y2_label)


    model = MyLogisticReg(X.shape[1])
    model.fit(X, Y)
    #prediction
    y_pred = model.predict(X)
    # evaluation
    accurate_rate = model.evaluate(Y.T, y_pred)
    print('The accurate_rate of my classifier is ' + str(accurate_rate))

if __name__ == '__main__':
    main()
