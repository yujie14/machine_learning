import numpy as np
import math
import re
from matplotlib import pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.neighbors import KNeighborsClassifier
from string import digits
from sklearn.model_selection import GridSearchCV

class KNN_WordEmbedding:
    """
    A class for document analysis using KNN with word embedding.
    """

    def __init__(self):
        pass



    def dict_Build(self):
        """
        read the glove.6B.50d.txt and store the dictionary in word_dict.
        :return: The dictionary of vector.
        """
        word_dict = {}
        for i, line in enumerate(open('glove.6B.50d.txt')):
            values = line.split()
            word_dict[values[0]] = np.asarray(values[1:], dtype = "float32")
        return word_dict



    def pre_Process_Train(self, text, document, label):
        """
        preprocess training data, split it into a list of document,
        remove digits, punctuationn, and convert upper case into lower case.
        :param text: the content of trainreviews.txt.
        :param document: store all statement of text.
        :param label: store all label of text
        """
        statement = text.split('\n')
        for i in range(len(statement) - 1):
            te = re.split('[\t\n]', statement[i])
            t = re.sub('[+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）]+', '', te[0])
            document.append(str.lower(t))
            label.append(te[1])



    def pre_Process_Test(self, text, exam):
        """
        preprocess test data, split it into a list of document,
        remove digits, punctuationn, and convert upper case into lower case.
        :param text: the content of testreviewsunlabeled.txt.
        :param exam: store all statement of text.
        """
        statement = text.split('\n')
        for i in range(len(statement) - 1):
            te = re.split('[\t\n]', statement[i])
            t = re.sub('[+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）]+', '', te[0])
            exam.append(str.lower(t))



    def Word_embedding(self, word_dict, document, exam):
        """
        Using word embedding to convert document into vector.
        :param word_dict: The dictionary of vector.
        :param document: a list of documents in training data, which is the feature of training data.
        :param exam: a list of documents in test data, which is the feature of test data.
        :return: The vector form of training data and test data.
        """

        # process the trainging data
        X = np.zeros((len(document), 50))
        for i in range(len(document)):
            for words in document[i].split():
                try:
                    X[i] = X[i] + word_dict[words]
                except KeyError:
                    pass

        # process the test data
        X_test = np.zeros((len(exam), 50))
        for i in range(len(exam)):
            for words in exam[i].split():
                try:
                    X_test[i] = X_test[i] + word_dict[words]
                except KeyError:
                    pass
        return[X, X_test]



    def KNN_Classifier_param_find(self, X, y):
        """
        Using GridSearchCV to find the best hyper-parameter and draw the plot
        :param X: The vector form of features of training data
        :param y: The label of training data
        :return: The best hyper-parameter with highest test accuracy.
        """

        # fing the best hyper-parameter.
        myKNN = KNeighborsClassifier()
        tuned_parameters = {'n_neighbors': [5, 10, 20, 30, 40, 50]}
        clf = GridSearchCV(myKNN, tuned_parameters, cv=10, scoring='accuracy', return_train_score=True)
        clf.fit(X, y)

        # store the candidates of hyper-parameter and error-rate in the list.
        parameter = []
        train_accuracy = clf.cv_results_['mean_train_score']
        test_accuracy = clf.cv_results_['mean_test_score']
        para = clf.cv_results_['params']
        for p in para:
            parameter.append(str(p['n_neighbors']))

        # draw the plot
        plt.suptitle("KNN_WordEmbedding")
        plt.xlabel("K")
        plt.ylabel("errorRate")
        l1, = plt.plot(parameter, 1-train_accuracy, label = "train_error_rate")
        l2, = plt.plot(parameter, 1-test_accuracy, label="test_error_rate")
        plt.legend(handles=[l1, l2,], labels = ["train_error_rate", "test_error_rate"], loc = 'best')
        plt.show()
        print(train_accuracy)
        print(test_accuracy)
        return clf.best_params_



    def KNN_Classifier_fit(self, para, X, y, X_test):
        """
        Using the best hyper-parameter to fit the model,
        calculate the confidence with mean and std,
        prediction for testdata.
        :param para: The best hyper-parameter.
        :param X: The vector form of features of training data.
        :param y: The label of training data.
        :param X_test: The vector form of features of test data.
        :return: The result of the prediction for test data.
        """

        # using cross-validation to calculate the mean and std of test accuracy.
        myKNN = KNeighborsClassifier(**para)
        clf = GridSearchCV(myKNN, {}, cv=10, scoring='accuracy')
        clf.fit(X, y)
        test_accuracy = clf.cv_results_['mean_test_score'][0]
        test_std = clf.cv_results_['std_test_score'][0]
        print("[mean - 1.96 * std, mean + 1.96 * std")
        print("[", test_accuracy, "- 1.96 * ", test_std, ", ", test_accuracy, "+ 1.96 * ", test_std, "]")
        print("[", test_accuracy - 1.96 * test_std, ", ", test_accuracy + 1.96 * test_std, "]")

        # fit the model and predict the test data.
        f = KNeighborsClassifier(**para)
        f.fit(X, y)
        return f.predict(X_test)



    def output(self, result):
        """
        write the result of the prediction for test data into the file.
        :param result: The result of the prediction for test data.
        """
        file = open("KNN_WE.txt", 'w')
        o = ''
        for i in range(len(result)):
            o = o + result[i] + '\n'
        file.write(o)
        file.close()



def main():
    # build the dictionary.
    model = KNN_WordEmbedding()
    word_dict = model.dict_Build()

    # read the training data and test data.
    filename_train = "trainreviews.txt"
    file_train = open(filename_train, "rt")
    filename_test = "testreviewsunlabeled.txt"
    file_test = open(filename_test, "rt")
    document = []
    label = []
    exam = []
    text_train = file_train.read()
    text_test = file_test.read()
    file_train.close()
    file_test.close()

    # preprocess the data.
    model.pre_Process_Train(text_train, document, label)
    model.pre_Process_Test(text_test, exam)
    [X, X_test] = model.Word_embedding(word_dict, document, exam)

    # find the best hyper-parameter.
    para = model.KNN_Classifier_param_find(X, label)
    print(para)

    # predict for the test data.
    result = model.KNN_Classifier_fit(para, X, label, X_test)
    model.output(result)



if __name__ == '__main__':
    main()